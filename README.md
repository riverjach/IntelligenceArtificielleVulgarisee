# IntelligenceArtificielleVulgarisee  

**ch3 Les fondamentaux du langage Python**  
**ch4 Des statistiques pour comprendre les données**  
**ch6 Machine Learning et Pokémons : première partie --> Pokemons**  
**ch7 Machine Learning et Pokémons : seconde partie --> Pokemons**  
**ch8 Bien classifier n'est pas une option --> AnalyseSonarMine**  
**ch9 Opinions et classification de textes --> classificationTexte**  
**ch10 Abricots, cerises et clustering --> AbricotsCerisesClustering**  
**ch11 Un neurone pour prédire**  
**ch12 Utilisation de plusieurs couches de neurones**  
**ch13 La classification d'images**  
**ch14 Votre ordinateur sait lire !**  
**ch15 Hommage au premier ChatBot**  

